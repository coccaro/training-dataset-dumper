#ifndef GET_JET_LINK_WRITER_HH
#define GET_JET_LINK_WRITER_HH

class IJetLinkWriter;
class JetLinkWriterConfig;

namespace H5 {
  class Group;
}

#include <memory>

std::unique_ptr<IJetLinkWriter> getJetLinkWriter(
  H5::Group&,
  const JetLinkWriterConfig&);


#endif
