#ifndef LEPTON_INFO_DECORATOR_HH
#define LEPTON_INFO_DECORATOR_HH

#include "xAODMuon/MuonContainer.h"

#include "xAODBTagging/BTaggingContainerFwd.h"
#include "xAODJet/JetContainerFwd.h"
#include "xAODTruth/TruthParticleFwd.h"

class LeptonTruthDecorator {
 public:
  using AE = SG::AuxElement;
  using PartLink = ElementLink<xAOD::IParticleContainer>;
  using PartLinks = std::vector<PartLink>;

  LeptonTruthDecorator();

  void decorate(const xAOD::BTagging& btag,
                const xAOD::Jet& jet) const;

 private:
  AE::ConstAccessor<ElementLink<xAOD::MuonContainer>> m_acc_softMuon_link;

  AE::ConstAccessor<int> m_acc_softMuon_truthOrigin;
  AE::ConstAccessor<int> m_acc_softMuon_truthType;

  AE::Decorator<int> m_deco_softMuon_truthOrigin;
  AE::Decorator<int> m_deco_softMuon_truthType;

};
#endif
