#ifndef TRACK_TRUTH_DECORATOR_HH
#define TRACK_TRUTH_DECORATOR_HH

#include "TrackSelector.hh"

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include <string>
#include <math.h>


// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackTruthDecorator
{
public:
  TrackTruthDecorator(const std::string& decorator_prefix = "");

  // this is the function that actually does the decoration
  void decorateAll(TrackSelector::Tracks tracks) const;

  // helper functions
  const xAOD::TruthParticle* getTruth( const xAOD::TrackParticle* track ) const;
  
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  SG::AuxElement::Decorator<int> m_track_truth_barcode;
  SG::AuxElement::Decorator<int> m_track_parent_barcode;

  typedef ElementLink<xAOD::TruthParticleContainer> TruthLink;
  SG::AuxElement::ConstAccessor<TruthLink> m_truthLink;
  SG::AuxElement::ConstAccessor<int> m_truthLabel;

};

#endif
